﻿using BrightIdeasSoftware;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Game_Updates
{
    public partial class Main_Window : About_System_Menu
    {
        Check_PKG hasher;
        Download_Manager downloader;
        public delegate void UpdateProgressBarDelegate(int progress, string filename);

        Get_Updates.Updates[] updates;
        Get_Updates updatesChecker;

        public const string DOWNLOADS_FOLDER = "Updates";
        private Status status;

        enum Status
        {
            Busy,
            Idle,
        };

        public Main_Window()
        {
            if (!File.Exists("no autoupdate"))
            {
                Autoupdate autoupdate = new Autoupdate();
                autoupdate.Owner = this;
            }

            InitializeComponent();
            createObjectListViewWidgets();

            downloader = new Download_Manager();
            hasher = new Check_PKG();
            updatesChecker = new Get_Updates();

            downloader.DownloadProgress += new EventHandler(download_manager_downloadStarted);
            hasher.HashingProgress += new EventHandler(check_pkg_hashingStarted);
            
            string entry = Clipboard.GetText();
            entry = entry.Replace("-", "");
            
            if ((entry.Length == 9) && (Regex.IsMatch(entry, @"^[A-Z]{4}[\d]{5}$")))
            {
                entryTextBox.Text = entry;
                checkBtn.Enabled = true;
                this.Load += new EventHandler(this.checkBtn_Click); // Send this button's click - PerformClick() does not work on load
            }
            
            status = Status.Idle;
        }

        private void clearBtn_Click(object sender, EventArgs e)
        {
            entryTextBox.Clear();
            entryTextBox.Focus();

            objectListView1.ClearObjects();
        }

        private void openDLFolderBtn_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(DOWNLOADS_FOLDER))
                Directory.CreateDirectory(DOWNLOADS_FOLDER);

            Process.Start(DOWNLOADS_FOLDER);
        }

        private void checkBtn_Click(object sender, EventArgs e)
        {
            if (rangeCheckBox.Checked)
            {
                string[] titleidRange = entryTextBox.Text.Split('-');

                string firstTitle = entryTextBox.Text.Substring(0, 4);
                int firstId = Int32.Parse(entryTextBox.Text.Substring(4, 5));
                string secondTitle = entryTextBox.Text.Substring(10, 4);
                int secondId = Int32.Parse(entryTextBox.Text.Substring(14, 5));

                if (!firstTitle.Equals(secondTitle))
                {
                    MessageBox.Show("WARNING: Both TitleIDs must belong to the same region.\nCheck aborted.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                if (firstId > secondId) // If the first ID has a larger value, swap them
                {
                    int tempInt = firstId;
                    firstId = secondId;
                    secondId = tempInt;
                }

                var range = Enumerable.Range(firstId, (secondId - firstId) + 1);

                foreach (var id in range)
                {
                    updates = updatesChecker.Fetch(firstTitle + id.ToString("00000"));
                    objectListView1.AddObjects(updates);
                }
            }
            else
            {
                string titleId = entryTextBox.Text;

                updates = updatesChecker.Fetch(titleId);
                objectListView1.AddObjects(updates);
            }
        }

        private void entryTextBox_TextChanged(object sender, EventArgs e)
        {
            if (Regex.IsMatch(entryTextBox.Text, @"^[A-Z]{4}[\d]{5}$") && !rangeCheckBox.Checked)
                checkBtn.Enabled = true;
            else if (Regex.IsMatch(entryTextBox.Text, @"^[A-Z]{4}[\d]{5}\-[A-Z]{4}[\d]{5}$") && rangeCheckBox.Checked)
                checkBtn.Enabled = true;
            else
                checkBtn.Enabled = false;
        }

        private void entryTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                checkBtn.PerformClick();
        }

        private void objectListView1_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Control) && (e.KeyCode == Keys.C))
            {
                string n = Environment.NewLine, title = string.Empty, version = string.Empty;
                string titleid, firmware, size, hash, url;
                ulong bytesize;
                
                StringBuilder sb = new StringBuilder();
                Toast_Form balloon;

                for (int i = 0; i < objectListView1.SelectedItems.Count; i++)
                {
                    var subitem = objectListView1.SelectedItems[i].SubItems;

                    version = subitem[0].Text;
                    titleid = subitem[1].Text;
                    title = subitem[2].Text;
                    firmware = subitem[3].Text;
                    size = subitem[4].Text;
                    hash = subitem[5].Text;
                    url = subitem[6].Text;
                    bytesize = ulong.Parse(subitem[7].Text);

                    sb.Append(titleid + n + title + n + version + n + "FW: " + firmware + n + size + " (" + string.Format("{0:#,#}", bytesize) + " B)" + n + url + n);
                }

                Clipboard.SetText(sb.ToString(), TextDataFormat.UnicodeText);

                if (objectListView1.SelectedItems.Count > 1)
                    balloon = new Toast_Form(5000, objectListView1.SelectedItems.Count + " items copied");
                else
                    balloon = new Toast_Form(5000, title + " " + version + " copied");
                balloon.Height = 50;
                balloon.Show();
            }
        }

        private void objectListView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (status == Status.Idle && objectListView1.SelectedItem.SubItems[6].Text.Length > 10)
            {
                status = Status.Busy;
                cancelDLBtn.Enabled = true;

                if (!Directory.Exists(DOWNLOADS_FOLDER))
                    Directory.CreateDirectory(DOWNLOADS_FOLDER);

                downloader.Download(objectListView1.SelectedItem.SubItems[6].Text, DOWNLOADS_FOLDER);
            }
        }

        private void cancelDLBtn_Click(object sender, EventArgs e)
        {
            downloader.continueWorking = false;
            status = Status.Idle;
            cancelDLBtn.Enabled = false;
        }

        private void createObjectListViewWidgets()
        {
            hashColumn.FreeSpaceProportion = 40;
            urlColumn.FreeSpaceProportion = 60;

            TextOverlay textOverlay = this.objectListView1.EmptyListMsgOverlay as TextOverlay;
            textOverlay.TextColor = Color.FromArgb(0x73, 0x2C, 0x7B);
            textOverlay.BackColor = Color.FromArgb(0xDD, 0xCA, 0xE9);
            textOverlay.BorderColor = Color.FromArgb(0x42, 0x1C, 0x52);
            textOverlay.BorderWidth = 3;
            textOverlay.Font = new Font("Verdana", 25);
            textOverlay.Rotation = 0;

            sizeColumn.AspectToStringConverter = delegate(object x)
            {
                long size = (long)x;

                if (size == 0)
                    return "";

                int[] limits = new int[] { 1024 * 1024 * 1024, 1024 * 1024, 1024 };
                string[] units = new string[] { "GiB", "MiB", "KiB" };

                for (int i = 0; i < limits.Length; i++)
                {
                    if (size >= limits[i])
                        return string.Format("{0:#,##0.00} " + units[i], ((double)size / limits[i]));
                }

                return string.Format("{0} B", size);
            };

            titleidColumn.AspectToStringConverter = delegate(object x)
            {
                string titleid = (string)x;

                if (!string.IsNullOrEmpty(titleid))
                    return titleid;

                return entryTextBox.Text;
            };

            // The column must be groupable for this to work
            versionColumn.GroupKeyGetter = delegate(object x)
            {
                Get_Updates.Updates update = (Get_Updates.Updates)x;

                string groupInfo = update.title; // Set the grouping name to the title

                return groupInfo;
            };

            versionColumn.GroupKeyToTitleConverter = delegate(object x)
            {
                return (string)x;
            };
        }
        
        private void download_manager_downloadStarted(object sender, EventArgs e)
        {
            if (e is Download_ManagerEventArgs)
            {
                Download_ManagerEventArgs download_ManagerEventArgs = e as Download_ManagerEventArgs;
                
                Invoke(new UpdateProgressBarDelegate(updateProgressBar), new object [] { download_ManagerEventArgs.Progress, download_ManagerEventArgs.Filename });
            }
        }

        private void check_pkg_hashingStarted(object sender, EventArgs e)
        {
            if (e is Check_PKGEventArgs)
            {
                Check_PKGEventArgs check_PKGEventArgs = e as Check_PKGEventArgs;

                Invoke(new UpdateProgressBarDelegate(updateProgressBar), new object[] { check_PKGEventArgs.Progress, string.Empty });
            }
        }

        private void hash(string filename)
        {
            string hashCheck;
            string sizeCheck = hasher.CheckSize(DOWNLOADS_FOLDER + "\\" + filename, true);
            
            if (sizeCheck.Contains("Error"))
            {
                Toast_Form balloon = new Toast_Form(8000, string.Format("Error on {0}", filename.Truncate(20)));
                balloon.Height = 50;
                balloon.Show();
            }
            else
            {
                hashCheck = hasher.CheckPKG(DOWNLOADS_FOLDER + "\\" + filename, false, true);

                if (hashCheck.Contains("BAD HASH"))
                {
                    Toast_Form balloon = new Toast_Form(8000, string.Format("Error on {0}", filename.Truncate(20)));
                    balloon.Height = 50;
                    balloon.Show();
                }
                else
                {
                    Toast_Form balloon = new Toast_Form(8000, string.Format("{0}\nwas successfully downloaded", filename.Truncate(30)));
                    balloon.Height = 50;
                    balloon.Show();
                }
            }
            
            status = Status.Idle;
        }

        private void updateProgressBar(int progress, string filename)
        {
            progressBar.Value = progress;
            
            if (progressBar.Value == 100)
            {
                progressBar.Value = 0; // Empty the progress bar after the download is complete, but only if it's 100% - otherwise it's too aggressive
                cancelDLBtn.Enabled = false;
            }

            if (!string.IsNullOrEmpty(filename))
            {
                hash(filename);
            }
        }

        private void rangeCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (rangeCheckBox.Checked)
            {
                entryTextBox.MaxLength = 19;
                if (entryTextBox.TextLength == 9)
                    checkBtn.Enabled = false;
            }
            else
            {
                entryTextBox.MaxLength = 9;
            }
        }

        private void objectListView1_CellRightClick(object sender, CellRightClickEventArgs e)
        {
            try
            {
                string currentSelection = e.SubItem.Text;

                Clipboard.SetText(currentSelection, TextDataFormat.UnicodeText);

                Toast_Form balloon = new Toast_Form(5000,  currentSelection + " copied.");
                balloon.Height = 50;
                balloon.Show();
            }
            catch (NullReferenceException)
            {
                Toast_Form balloon = new Toast_Form(5000, "The item wasn't copied.");
                balloon.Height = 50;
                balloon.Show();
            }
        }
    }
}

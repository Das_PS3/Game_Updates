﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Game_Updates
{
    class Download_Manager
    {
        public event EventHandler DownloadProgress;
        Download_ManagerEventArgs download_ManagerEventArgs;

        public void DownloadStarted(Download_ManagerEventArgs e)
        {
            EventHandler downloadProgress = DownloadProgress;
            if (downloadProgress != null)
                downloadProgress(this, e);
        }

        static Stopwatch sw = new Stopwatch();

        private long downloaded;
        private long size;

        private const byte DL_PARTS = 3;
        private const long MAX_FILE_SIZE = 1024 * 1024 * 50; // 50 MiB
        private const string U_A = "I'll grab this... -D";

        public volatile bool continueWorking = true;

        public void Download(string address, string downDir)
        {
            Thread thread = new Thread(() =>
            {
                downDir += @"\";
                long request_size;
                ServicePointManager.DefaultConnectionLimit = 5;

                HttpWebRequest header = (HttpWebRequest)WebRequest.Create(address);
                header.Method = "HEAD"; // Defaults to GET
                header.UserAgent = U_A;

                using (HttpWebResponse headerResponse = (HttpWebResponse)header.GetResponse())
                {
                    size = headerResponse.ContentLength;
                    request_size = (long)Math.Round(size / (double)DL_PARTS, 0);
                }

                string filename = Path.GetFileName(header.RequestUri.AbsolutePath);

                sw.Start();

                if (size > MAX_FILE_SIZE)
                {
                    Task<string>[] parts = new Task<string>[DL_PARTS];
                    parts[0] = Task.Factory.StartNew(() => DownloadPartFile(address, 0, request_size));
                    parts[1] = Task.Factory.StartNew(() => DownloadPartFile(address, request_size + 1, request_size * 2));
                    parts[2] = Task.Factory.StartNew(() => DownloadPartFile(address, request_size * 2 + 1, size));
                    Task.WaitAll(parts);

                    if (continueWorking)
                    {
                        //string averageDlSpeed = (size / 1024d / sw.Elapsed.TotalSeconds).ToString("0.00");
                        //string time = (sw.Elapsed.Minutes + sw.Elapsed.Seconds).ToString("0:00");

                        sw.Reset();

                        var tempFiles = new string[DL_PARTS];
                        for (int i = 0; i < parts.Length; i++)
                            tempFiles[i] = parts[i].Result;

                        if (File.Exists(downDir + filename))
                            File.Delete(downDir + filename);
                            
                        ConcatenateFile(downDir + filename, tempFiles);
                    }
                    else
                        foreach (Task<string> t in parts)
                            File.Delete(t.Result);
                }
                else
                {
                    Task<MemoryStream>[] parts = new Task<MemoryStream>[DL_PARTS];
                    parts[0] = Task.Factory.StartNew(() => DownloadPartRAM(address, 0, request_size));
                    parts[1] = Task.Factory.StartNew(() => DownloadPartRAM(address, request_size + 1, request_size * 2));
                    parts[2] = Task.Factory.StartNew(() => DownloadPartRAM(address, request_size * 2 + 1, size));
                    Task.WaitAll(parts);

                    if (continueWorking)
                    {
                        //string averageDlSpeed = (size / 1024d / sw.Elapsed.TotalSeconds).ToString("0.00");
                        //string time = (sw.Elapsed.Minutes + sw.Elapsed.Seconds).ToString("0:00");

                        sw.Reset();

                        var tempRAMData = new MemoryStream[DL_PARTS];
                        for (int i = 0; i < parts.Length; i++)
                            tempRAMData[i] = parts[i].Result;

                        if (File.Exists(downDir + filename))
                            File.Delete(downDir + filename);
                            
                        ConcatenateRAM(downDir + filename, tempRAMData);
                    }
                }

                download_ManagerEventArgs = continueWorking ? new Download_ManagerEventArgs(0, filename) : new Download_ManagerEventArgs(0);
                DownloadStarted(download_ManagerEventArgs);

                downloaded = 0; // Reset this after the download is done. Otherwise, future downloads will report progress starting at 101%
                continueWorking = true; // Reset back to true if it's been cancelled, otherwise, all future downloads will fail
            });
            thread.IsBackground = true; // Kill the download if the application is closed
            thread.Start();
        }

        string DownloadPartFile(string address, long start, long end)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(address);
            request.UserAgent = U_A;
            request.AddRange(start, end);

            byte[] readBuffer = new byte[32 * 1024];

            string tempFile = Path.GetTempFileName();

            using (WebResponse response = request.GetResponse())
            using (Stream data = response.GetResponseStream())
            using (BinaryWriter writer = new BinaryWriter(File.Create(tempFile)))
            {
                int readBytesCount;
                while ((readBytesCount = data.Read(readBuffer, 0, readBuffer.Length)) > 0)
                {
                    writer.Write(readBuffer, 0, readBytesCount);
                    downloaded += readBytesCount;
                    int progress = (int)((downloaded * 100) / size);
                    download_ManagerEventArgs = new Download_ManagerEventArgs(progress);
                    DownloadStarted(download_ManagerEventArgs);

                    if (!continueWorking)
                        break;
                }
            }

            return tempFile;
        }

        MemoryStream DownloadPartRAM(string address, long start, long end)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(address);
            request.UserAgent = U_A;
            request.AddRange(start, end);

            byte[] readBuffer = new byte[32 * 1024];
            MemoryStream ramData = new MemoryStream(); // Although MemoryStream implements IDisposable (inherited from the base class), it doesn't hold any resources

            using (WebResponse response = request.GetResponse())
            using (Stream data = response.GetResponseStream())
            {
                int readBytesCount;
                while ((readBytesCount = data.Read(readBuffer, 0, readBuffer.Length)) > 0)
                {
                    ramData.Write(readBuffer, 0, readBytesCount);
                    downloaded += readBytesCount;
                    int progress = (int)((downloaded * 100) / size);
                    download_ManagerEventArgs = new Download_ManagerEventArgs(progress);
                    DownloadStarted(download_ManagerEventArgs);

                    if (!continueWorking)
                        break;
                }
            }

            return ramData;
        }

        void ConcatenateFile(string outputFile, IEnumerable<string> inputFiles)
        {
            using (FileStream output = new FileStream(outputFile, FileMode.Create, FileAccess.Write))
                foreach (string inputFile in inputFiles)
                {
                    using (FileStream input = new FileStream(inputFile, FileMode.Open, FileAccess.Read))
                        input.CopyTo(output);

                    File.Delete(inputFile);
                }
        }

        void ConcatenateRAM(string outputFile, IEnumerable<MemoryStream> inputData)
        {
            using (FileStream output = new FileStream(outputFile, FileMode.Create, FileAccess.Write))
                foreach (MemoryStream input in inputData)
                    input.WriteTo(output);
        }
    }



    class Download_ManagerEventArgs : EventArgs
    {
        public string Filename { get; private set; }
        public int Progress { get; private set; }

        public Download_ManagerEventArgs(int progress, string filename)
        {
            Progress = progress;
            Filename = filename;
        }
        public Download_ManagerEventArgs(int progress)
            : this(progress, string.Empty)
        {
            Progress = progress;
        }
    }
}

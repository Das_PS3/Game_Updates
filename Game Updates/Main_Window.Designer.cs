﻿namespace Game_Updates
{
    partial class Main_Window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_Window));
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.openDLFolderBtn = new System.Windows.Forms.Button();
            this.cancelDLBtn = new System.Windows.Forms.Button();
            this.clearBtn = new System.Windows.Forms.Button();
            this.checkBtn = new System.Windows.Forms.Button();
            this.entryTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.objectListView1 = new BrightIdeasSoftware.ObjectListView();
            this.versionColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.titleidColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.titleColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.fwColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.sizeColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.hashColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.urlColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.sizeBytes = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.rangeCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.objectListView1)).BeginInit();
            this.SuspendLayout();
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(12, 346);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(775, 23);
            this.progressBar.TabIndex = 7;
            // 
            // openDLFolderBtn
            // 
            this.openDLFolderBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.openDLFolderBtn.Location = new System.Drawing.Point(672, 8);
            this.openDLFolderBtn.Name = "openDLFolderBtn";
            this.openDLFolderBtn.Size = new System.Drawing.Size(115, 23);
            this.openDLFolderBtn.TabIndex = 5;
            this.openDLFolderBtn.Text = "Open updates folder";
            this.openDLFolderBtn.UseVisualStyleBackColor = true;
            this.openDLFolderBtn.Click += new System.EventHandler(this.openDLFolderBtn_Click);
            // 
            // cancelDLBtn
            // 
            this.cancelDLBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelDLBtn.Enabled = false;
            this.cancelDLBtn.Location = new System.Drawing.Point(551, 8);
            this.cancelDLBtn.Name = "cancelDLBtn";
            this.cancelDLBtn.Size = new System.Drawing.Size(115, 23);
            this.cancelDLBtn.TabIndex = 4;
            this.cancelDLBtn.Text = "Cancel download";
            this.cancelDLBtn.UseVisualStyleBackColor = true;
            this.cancelDLBtn.Click += new System.EventHandler(this.cancelDLBtn_Click);
            // 
            // clearBtn
            // 
            this.clearBtn.Location = new System.Drawing.Point(371, 8);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(75, 23);
            this.clearBtn.TabIndex = 3;
            this.clearBtn.Text = "Clear";
            this.clearBtn.UseVisualStyleBackColor = true;
            this.clearBtn.Click += new System.EventHandler(this.clearBtn_Click);
            // 
            // checkBtn
            // 
            this.checkBtn.Enabled = false;
            this.checkBtn.Location = new System.Drawing.Point(290, 8);
            this.checkBtn.Name = "checkBtn";
            this.checkBtn.Size = new System.Drawing.Size(75, 23);
            this.checkBtn.TabIndex = 2;
            this.checkBtn.Text = "Check";
            this.checkBtn.UseVisualStyleBackColor = true;
            this.checkBtn.Click += new System.EventHandler(this.checkBtn_Click);
            // 
            // entryTextBox
            // 
            this.entryTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.entryTextBox.Location = new System.Drawing.Point(63, 10);
            this.entryTextBox.MaxLength = 9;
            this.entryTextBox.Name = "entryTextBox";
            this.entryTextBox.Size = new System.Drawing.Size(128, 20);
            this.entryTextBox.TabIndex = 1;
            this.entryTextBox.TextChanged += new System.EventHandler(this.entryTextBox_TextChanged);
            this.entryTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.entryTextBox_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Title ID:";
            // 
            // objectListView1
            // 
            this.objectListView1.AllColumns.Add(this.versionColumn);
            this.objectListView1.AllColumns.Add(this.titleidColumn);
            this.objectListView1.AllColumns.Add(this.titleColumn);
            this.objectListView1.AllColumns.Add(this.fwColumn);
            this.objectListView1.AllColumns.Add(this.sizeColumn);
            this.objectListView1.AllColumns.Add(this.hashColumn);
            this.objectListView1.AllColumns.Add(this.urlColumn);
            this.objectListView1.AllColumns.Add(this.sizeBytes);
            this.objectListView1.AllowColumnReorder = true;
            this.objectListView1.AlternateRowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.objectListView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.objectListView1.CheckedAspectName = "";
            this.objectListView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.versionColumn,
            this.titleidColumn,
            this.titleColumn,
            this.fwColumn,
            this.sizeColumn,
            this.hashColumn,
            this.urlColumn,
            this.sizeBytes});
            this.objectListView1.CopySelectionOnControlC = false;
            this.objectListView1.CopySelectionOnControlCUsesDragSource = false;
            this.objectListView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.objectListView1.EmptyListMsg = "Enter a TitleID to search for updates";
            this.objectListView1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.objectListView1.FullRowSelect = true;
            this.objectListView1.LabelWrap = false;
            this.objectListView1.Location = new System.Drawing.Point(12, 37);
            this.objectListView1.Name = "objectListView1";
            this.objectListView1.SelectColumnsOnRightClick = false;
            this.objectListView1.SelectColumnsOnRightClickBehaviour = BrightIdeasSoftware.ObjectListView.ColumnSelectBehaviour.None;
            this.objectListView1.ShowFilterMenuOnRightClick = false;
            this.objectListView1.ShowItemCountOnGroups = true;
            this.objectListView1.Size = new System.Drawing.Size(775, 303);
            this.objectListView1.SortGroupItemsByPrimaryColumn = false;
            this.objectListView1.TabIndex = 8;
            this.objectListView1.TintSortColumn = true;
            this.objectListView1.UseAlternatingBackColors = true;
            this.objectListView1.UseCompatibleStateImageBehavior = false;
            this.objectListView1.UseHotItem = true;
            this.objectListView1.UseHyperlinks = true;
            this.objectListView1.UseTranslucentHotItem = true;
            this.objectListView1.UseTranslucentSelection = true;
            this.objectListView1.View = System.Windows.Forms.View.Details;
            this.objectListView1.CellRightClick += new System.EventHandler<BrightIdeasSoftware.CellRightClickEventArgs>(this.objectListView1_CellRightClick);
            this.objectListView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.objectListView1_KeyDown);
            this.objectListView1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.objectListView1_MouseDoubleClick);
            // 
            // versionColumn
            // 
            this.versionColumn.AspectName = "version";
            this.versionColumn.AspectToStringFormat = "";
            this.versionColumn.CellPadding = null;
            this.versionColumn.DisplayIndex = 2;
            this.versionColumn.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.versionColumn.Hideable = false;
            this.versionColumn.MaximumWidth = 60;
            this.versionColumn.MinimumWidth = 60;
            this.versionColumn.Text = "Version";
            this.versionColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // titleidColumn
            // 
            this.titleidColumn.AspectName = "titleid";
            this.titleidColumn.CellPadding = null;
            this.titleidColumn.DisplayIndex = 0;
            this.titleidColumn.Groupable = false;
            this.titleidColumn.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.titleidColumn.Hideable = false;
            this.titleidColumn.MaximumWidth = 90;
            this.titleidColumn.MinimumWidth = 90;
            this.titleidColumn.Text = "TitleID";
            this.titleidColumn.Width = 90;
            // 
            // titleColumn
            // 
            this.titleColumn.AspectName = "title";
            this.titleColumn.CellPadding = null;
            this.titleColumn.DisplayIndex = 1;
            this.titleColumn.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.titleColumn.Hideable = false;
            this.titleColumn.MaximumWidth = 310;
            this.titleColumn.MinimumWidth = 210;
            this.titleColumn.Text = "Title";
            this.titleColumn.Width = 210;
            // 
            // fwColumn
            // 
            this.fwColumn.AspectName = "firmware";
            this.fwColumn.AspectToStringFormat = "";
            this.fwColumn.CellPadding = null;
            this.fwColumn.Groupable = false;
            this.fwColumn.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.fwColumn.Hideable = false;
            this.fwColumn.MaximumWidth = 80;
            this.fwColumn.MinimumWidth = 80;
            this.fwColumn.Text = "FW Version";
            this.fwColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.fwColumn.Width = 80;
            // 
            // sizeColumn
            // 
            this.sizeColumn.AspectName = "size";
            this.sizeColumn.AspectToStringFormat = "{0:0.00}";
            this.sizeColumn.CellPadding = null;
            this.sizeColumn.Groupable = false;
            this.sizeColumn.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.sizeColumn.Hideable = false;
            this.sizeColumn.MinimumWidth = 75;
            this.sizeColumn.Text = "Size";
            this.sizeColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.sizeColumn.Width = 75;
            // 
            // hashColumn
            // 
            this.hashColumn.AspectName = "hash";
            this.hashColumn.CellPadding = null;
            this.hashColumn.FillsFreeSpace = true;
            this.hashColumn.Groupable = false;
            this.hashColumn.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.hashColumn.Hideable = false;
            this.hashColumn.MaximumWidth = 320;
            this.hashColumn.MinimumWidth = 150;
            this.hashColumn.Sortable = false;
            this.hashColumn.Text = "Hash";
            this.hashColumn.Width = 200;
            // 
            // urlColumn
            // 
            this.urlColumn.AspectName = "link";
            this.urlColumn.CellPadding = null;
            this.urlColumn.FillsFreeSpace = true;
            this.urlColumn.Groupable = false;
            this.urlColumn.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.urlColumn.Hideable = false;
            this.urlColumn.Hyperlink = true;
            this.urlColumn.MinimumWidth = 100;
            this.urlColumn.Sortable = false;
            this.urlColumn.Text = "URL";
            this.urlColumn.Width = 100;
            // 
            // sizeBytes
            // 
            this.sizeBytes.AspectName = "size";
            this.sizeBytes.CellPadding = null;
            this.sizeBytes.Groupable = false;
            this.sizeBytes.Hideable = false;
            this.sizeBytes.IsEditable = false;
            this.sizeBytes.MaximumWidth = 0;
            this.sizeBytes.MinimumWidth = 0;
            this.sizeBytes.Searchable = false;
            this.sizeBytes.ShowTextInHeader = false;
            this.sizeBytes.Sortable = false;
            this.sizeBytes.UseFiltering = false;
            this.sizeBytes.Width = 0;
            // 
            // rangeCheckBox
            // 
            this.rangeCheckBox.AutoSize = true;
            this.rangeCheckBox.Location = new System.Drawing.Point(197, 12);
            this.rangeCheckBox.Name = "rangeCheckBox";
            this.rangeCheckBox.Size = new System.Drawing.Size(87, 17);
            this.rangeCheckBox.TabIndex = 9;
            this.rangeCheckBox.Text = "Check range";
            this.rangeCheckBox.UseVisualStyleBackColor = true;
            this.rangeCheckBox.CheckedChanged += new System.EventHandler(this.rangeCheckBox_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 372);
            this.Controls.Add(this.rangeCheckBox);
            this.Controls.Add(this.objectListView1);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.openDLFolderBtn);
            this.Controls.Add(this.cancelDLBtn);
            this.Controls.Add(this.clearBtn);
            this.Controls.Add(this.checkBtn);
            this.Controls.Add(this.entryTextBox);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(815, 410);
            this.Name = "Form1";
            this.Text = "Game Updates";
            ((System.ComponentModel.ISupportInitialize)(this.objectListView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox entryTextBox;
        private System.Windows.Forms.Button checkBtn;
        private System.Windows.Forms.Button clearBtn;
        private System.Windows.Forms.Button cancelDLBtn;
        private System.Windows.Forms.Button openDLFolderBtn;
        private System.Windows.Forms.ProgressBar progressBar;
        public System.Windows.Forms.Label label1;
        private BrightIdeasSoftware.OLVColumn titleidColumn;
        private BrightIdeasSoftware.ObjectListView objectListView1;
        private BrightIdeasSoftware.OLVColumn titleColumn;
        private BrightIdeasSoftware.OLVColumn versionColumn;
        private BrightIdeasSoftware.OLVColumn fwColumn;
        private BrightIdeasSoftware.OLVColumn sizeColumn;
        private BrightIdeasSoftware.OLVColumn hashColumn;
        private BrightIdeasSoftware.OLVColumn urlColumn;
        private BrightIdeasSoftware.OLVColumn sizeBytes;
        private System.Windows.Forms.CheckBox rangeCheckBox;
    }
}

